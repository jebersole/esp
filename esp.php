<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Экстрасенсы</title>
	</head>
	<body>
        <?php
            define(ESP_NUM, 2);    
            if(isset($_POST['submit'])) {
                $input = filter_var($_POST['input'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                if (preg_match("/^[0-9]{2}$/", $input)) {
                    session_start();
                    $_SESSION['numbers'][] = $input;
                    for ($i = 0; $i < ESP_NUM; $i++) {
                        $_SESSION['esp'][$i]['num'] = rand(00,99);
                        $_SESSION['esp'][$i]['guessed'][] = $_SESSION['esp'][$i]['num'];
                    }                
                    session_write_close();
                } else {
                    $fail = true;
                }               
            } 
        ?>

        <h2 style="text-align: center;">Экстрасенсы</h2>   
        <?php if(isset($_POST['submit'])) { ?>
            <h3>История догадок</h3>
                <?php $count = 1; foreach ($_SESSION['esp'] as $esp) { ?>
                    ESP <?php echo $count; $count++; ?>
                    <ul>
                        <?php foreach ($esp['guessed'] as $guess) { ?>
                            <li><?=$guess?></li>
                        <?php } ?>
                    </ul>
                <?php } ?> 
        <?php } ?>       

        <form action="" method="post">
            <?php if(isset($_POST['submit'])) { ?>
                <h3>История введённых чисел</h3>
                    <ul>
                        <?php foreach ($_SESSION['numbers'] as $item) { echo '<li>'.$item.'</li>'; } ?>
                    </ul>
            <?php } ?>
            Пожалуйста, введите две цифры: <input type="text" name="input"/>
            <?php if($fail) { ?>
            <div style="color: red; ">Пожалуйста, введите только две цифры.</div><br/>
            <?php } ?>            
            <input type="submit" name="submit" value="Отправить"/>
        </form> 

        <?php if(isset($_POST['submit']) && !$fail) { ?>
            <h3>Достоверность</h3>
                <?php 
                    session_start();
                    for ($i = 0; $i < count($_SESSION['esp']); $i++) {
                        if ($_SESSION['esp'][$i]['num'] == $input) $_SESSION['esp'][$i]['dost'] += 1;
                        else $_SESSION['esp'][$i]['dost'] -= 1;
                    ?>
                    <h4>ESP <?=$i+1?>: <?=$_SESSION['esp'][$i]['dost']?></h4>
                <?php } session_write_close(); ?>
        <?php } ?>
    </body>
</html>
